const Stripe = require('stripe');
const stripe = Stripe('sk_test_51PEwawBYQqea3LZFnARTWx0YOFopVH6NyFXp8ZOf0yBTH3ROGTyKt7ZtzpSTP9wRZsCGk9J5IhvFUxCDnf8MHEoV009sKRnoQD');

(async () => {
    // Create a customer and an authorization.
    const customer = await stripe.customers.create({
        name: 'Jenny Rosen',
        email: 'jennyrosen@example.com',
    });

    console.log(`What is the customer ID? ${customer.id}...`);

    // Attach a successful test card to the customer.
    const paymentMethod = await stripe.paymentMethods.attach(
        "pm_card_visa",
        {
            customer: customer.id,
        }
    );

    // Create an authorization only charge for $33.00 USD with the credit card as the payment method.
    const paymentIntent = await stripe.paymentIntents.create({
        amount: 3300,
        currency: 'usd',
        customer: customer.id,
        payment_method: paymentMethod.id,
        capture_method: "manual",
        confirm: true,
        return_url: "http://localhost:8080"
    });

    // Now, capture the payment.
    const capture = await stripe.paymentIntents.capture(
        paymentIntent.id
    );

    console.log(`What is the charge ID? ${capture.latest_charge}...`);

    // Create a PaymentIntent to charge a customer $44 dollars that captures the charge at the time of creation.
    const instantPaymentIntent = await stripe.paymentIntents.create({
        amount: 4400,
        currency: 'usd',
        customer: customer.id,
        payment_method: paymentMethod.id,
        confirm: true,
        return_url: "http://localhost:8080",
    });

    // Create a Refund.
    await stripe.refunds.create({
        charge: instantPaymentIntent.latest_charge,
    });
})();