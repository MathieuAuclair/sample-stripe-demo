import stripe
from datetime import datetime
from flask import Flask, render_template, request, jsonify

app = Flask(__name__, template_folder="pages")
stripe.api_key = 'sk_test_51PEwawBYQqea3LZFnARTWx0YOFopVH6NyFXp8ZOf0yBTH3ROGTyKt7ZtzpSTP9wRZsCGk9J5IhvFUxCDnf8MHEoV009sKRnoQD'

customer_id = "cus_Q5V3NqvG55Go2X"

@app.route('/')
def index():
    return render_template('stripe-checkout.html')

@app.route('/Payment/GetSubscriptions', methods=["POST"])
def get_subscriptions():
    try:
        subscriptions = stripe.Subscription.list(customer=customer_id)

        return jsonify(subscriptions)
    except Exception as e:
        return jsonify({'error': str(e)}), 500
    
@app.route('/Payment/UpdateAnchor', methods=["POST"])
def update_anchor():
    try:
        subscription_id = request.form['id']
        
        stripe.Subscription.modify(
            subscription_id,
            billing_cycle_anchor='now'
        )

        return render_template('stripe-checkout.html')
    except Exception as e:
        return jsonify({'error': str(e)}), 500

@app.route('/Payment/CreateCheckoutSession', methods=['POST'])
def create_checkout_session():
    try:
        product = stripe.Product.create(
            name='One-time product',
            type='service'
        )

        price = stripe.Price.create(
            unit_amount=1234,
            currency='usd',
            product=product.id,
            recurring={'interval': 'month'}
        )

        checkout_session = stripe.checkout.Session.create(
            success_url="http://localhost:8080/",
            line_items=[{"price": price.id, "quantity": 1}],
            subscription_data={ 'billing_cycle_anchor': int(datetime.now().timestamp()) },
            mode="subscription",
            customer=customer_id
        )    

        return jsonify({'url': checkout_session.url})
    except Exception as e:
        return jsonify({'error': str(e)}), 500

if __name__ == '__main__':
    app.run(port=8080, debug=True)
