const path = require('path');
const Stripe = require('stripe');
const express = require('express');
const app = express();
const port = 8080;

const stripe = Stripe('sk_test_51PEwawBYQqea3LZFnARTWx0YOFopVH6NyFXp8ZOf0yBTH3ROGTyKt7ZtzpSTP9wRZsCGk9J5IhvFUxCDnf8MHEoV009sKRnoQD');


app.get('/', (request, response) => {
    response.sendFile(path.resolve('pages/stripe-element.html'));
})

app.post('/Payment/CreatePaymentIntent', async (request, response) => {
    const instantPaymentIntent = await stripe.paymentIntents.create({
        amount: 5000,
        currency: 'usd',
        automatic_payment_methods: {
            enabled: true,
        },
    });

    response.send({ client_secret: instantPaymentIntent.client_secret });
});

app.get('/Payment/Confirm', async (request, response) => {
    response.sendFile(path.resolve('pages/success.html'));
});

app.listen(port, () => {
    console.log(`App listening on port ${port}, open http://localhost:8080 in your browser...`);
})