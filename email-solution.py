from datetime import datetime
import stripe

stripe.api_key = 'sk_test_51PEwawBYQqea3LZFnARTWx0YOFopVH6NyFXp8ZOf0yBTH3ROGTyKt7ZtzpSTP9wRZsCGk9J5IhvFUxCDnf8MHEoV009sKRnoQD'

# An existing customer
customer_id = "cus_Q5V3NqvG55Go2X"

# Create streaming service
product = stripe.Product.create(
    name="Streaming Service",
    type="service",
)

# Create plan A
planA = stripe.Plan.create(
    amount_decimal=2499, 
    currency="usd",
    interval="month",
    product=product.id,
    nickname="Plan A - Unlimited",
    billing_scheme="per_unit",
)

# Create plan B
planB = stripe.Plan.create(
    amount=1099,
    currency="usd",
    interval="month",
    product=product.id,
    nickname="Plan B - Limited",
    billing_scheme="per_unit",
)

# Set meter for calculating data usage
meter = stripe.billing.Meter.create(
    display_name="GB Usage",
    event_name="gb_usage",
    default_aggregation={"formula": "sum"},
    customer_mapping={"event_payload_key": "stripe_customer_id", "type": "by_id"},
    value_settings={"event_payload_key": "value"}
)

# Calculated extra subsequent usage price
metered_price = stripe.Price.create(
    product=product.id,
    currency="usd",
    unit_amount=100,
    billing_scheme="per_unit",
    recurring={
        "usage_type": "metered",
        "interval": "month",
        "meter": meter.id
    },
    transform_quantity={
      "divide_by": 10,
      "round": "up"
    },
)

# Create a coupon
coupon = stripe.Coupon.create(
    percent_off=25,
    duration='once',
    id='25OFF',
    applies_to={
        'products': [product.id],
    },
)

# Get a payment method
payment_method = stripe.PaymentMethod.attach(
    "pm_card_visa",
    customer=customer_id,
)

# Create a subscription A, with coupon
subscriptionA = stripe.Subscription.create(
    customer=customer_id,
    default_payment_method=payment_method.id,
    items=[{'plan': planA}],
    coupon=coupon.id
)

# Create a subscription B, without coupon
subscriptionB = stripe.Subscription.create(
    customer=customer_id,
    default_payment_method=payment_method.id,
    items=[
        {'plan': planB},
        {'price': metered_price} # include charge for subsequent usage
    ]
)

# Register subsequent usage
stripe.billing.MeterEvent.create(
    event_name=meter.event_name,
    payload={
        "value": "2", # 2Gb over the limit of 100Gb per month, total of the subscription should be set a 11.99$ (10.99$ + 1.00$ extra) for the current month
        "stripe_customer_id": customer_id
    },
)