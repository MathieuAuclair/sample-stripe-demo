# Documentation

I did the assignment in NodeJS because it wasn't a huge project. NodeJS is perfect for scripting fast proof of concept and has acceptable performances.

## Setup

Setup was made on Ubuntu Linux, Windows might require a bit of tweaking, consider using chocolatey CLI to make your life easier.

```bash
# Installing nodejs 20.12.2 using the version manager 'n'
sudo apt-get install nodejs npm -y
sudo npm install -g n
sudo n 20.12.2

# Install required packages
npm install

# Install python3 and pip3
sudo apt install -y python3 python3-pip

# Install required packages
pip3 install flask stripe
```

## Running the test for the first part of the homework

The tests are located in different files, here's how to run each one.

### Part 1 to 3

Run node with the following file:

```bash
node part-1-to-3.js
```

The program output will be the questions in the document. You can also verify in the script each step, I added the steps in comment.

### Part 4

Run node with the following file:

```bash
node part-4.js
```

Then in the browser of your choice open the address [`localhost:8080`](http://localhost:8080). Else you may watch the following video as requested in the document:

<video width="320" height="240" controls>
  <source src="./demo.mkv">
</video>

[In case this video doesn't load, open it directly](./demo.mkv)

### Part 5

Run python3 with the following file:

```bash
python3 part-5.py
```

Then in the browser of your choice open the address [`localhost:8080`](http://localhost:8080).

## Running the test for the second part of the homework

The email response, is located in [./Proposal.md](Proposal.md), and the script to demonstrate the process can be ran with the following command:

```bash
python3 email-solution.py
```