# Streaming service billing plan

Hi there! 

Happy to hear that we're going forward with the streaming service! That being said, I uploaded a script for your developers showing how we could go about implementing the billing requirements with Stripe.

https://gitlab.com/MathieuAuclair/sample-stripe-demo/-/blob/master/email-solution.py

The script is written in python as it is the language that your organization uses in its environnement. 

The script creates 2 flat rate plans, 
- Plan A - 24.99 a month for unlimited usage
- Plan B - 10.99 a month for limited usage

In order to incur the usage charge for exceeding 100GB with Plan B, which is priced at $1.00 for each additional 10GB:

_By default, stripe does not support having both a flat rate and a metered usage within a single plan. Another problem is that the aggregations are quite limited for calculation. We can apply tiers, but it won't ignore the first 100Gb of usage once we go over the included limit and want to charge by the unit._

**A very simple solution in our case** is to include an extra price in the customer subscription that will be calculated based on a meter for which you can send events throughout the month.

Every time your system records subsequent usage, an event can be sent to the `MeterEvent` which will update the price.

Let me if I can provide further guidance on specifics! I'll be happy to help!

Have a nice day!

_Mathieu Auclair (Payment Solution Consultant @ Yeeld)_